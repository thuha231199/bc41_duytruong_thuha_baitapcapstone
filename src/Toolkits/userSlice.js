import { createSlice } from '@reduxjs/toolkit'
import { localUserServ } from '../Services/localService';

const initialState = { 
    userInfo : localUserServ.get() , 
}

const userSlice = createSlice({
    name: 'userSlice',
    initialState,
    reducers: {
        setUserInfo : (state , action) => {
            state.userInfo = action.payload
        },
        setLoginUser: (state, action) => {
            state.userInfo = action.payload;
        },
    }
});
// todo
export const {setUserInfo, setLoginUser} = userSlice.actions

export default userSlice.reducer