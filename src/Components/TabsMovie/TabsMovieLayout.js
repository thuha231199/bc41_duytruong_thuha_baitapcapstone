import React from 'react'
import { Desktop, LargeDesktop } from '../../Layout/Responsive'
import TabsMovie from './TabsMovie'

export default function TabsMovieLayout() {
  return (
    <div>
        <Desktop>
            <TabsMovie/>
        </Desktop>
        <LargeDesktop>
            <TabsMovie/>
        </LargeDesktop>
    </div>
  )
}
