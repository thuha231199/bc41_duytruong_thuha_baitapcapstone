import moment from 'moment/moment'
import React from 'react'

export default function ItemTabsMovie({phim}) {
  return (
    <div className='movieShowtime flex items-start'>
        <img className='' src={phim.hinhAnh} alt="" />
        <div className='detailShowtime'>
            <h5 className='flex justify-start items-center'>
              <span className='movieTag mr-2 '>C18</span>{phim.tenPhim}</h5>
            <div>
              {phim.lstLichChieuTheoPhim.slice(0,6).map((item) => {
                return <a className='showtime' href='#' key={item.maLichChieu}>
                          <span className='text-black'>{moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ")}</span>
                          ~
                          <span className='text-red-600'> {moment(item.ngayChieuGioChieu).format("hh:mm")}</span>
                       </a>
              })}
            </div>
        </div>
    </div>
  )
}
