import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Card , Button } from 'antd';
import 'animate.css';
import { useDispatch } from "react-redux";
import { handleOpenModal } from "../../Toolkits/modalSlice";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
function MovieSlider({movieList}) {
    let dispatch = useDispatch() ; 
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
            },
        },
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            },
        }
        ],
    };
    let handleOpen = (url) => {
        dispatch(handleOpenModal(url)) ; 
    }
    return (
            <Slider {...settings} className="sm:my-5 lg:my-20 h-full overflow-y-visible overflow-x-clip sm:overflow-x-clip sm:overflow-hidden md:overflow-visible lg:overflow-visible">
                {movieList.map((movie , index) => {
                    return (
                        <div key={index} className="h-full w-full z-50">
                            <Card
                            key={index}
                            hoverable
                            cover={<div className = "h-80 w-full lg:h-80 md:h-72 relative">
                            <img className="h-80 w-full lg:h-80 md:h-72 object-cover absolute top-0 left-0" alt="example" src={movie.hinhAnh} />
                            <i onClick={() => {handleOpen(movie.trailer)}} className="fa fa-play-circle text-white text-4xl w-20 h-20 border transition hover:scale-125 button__play button__listmovie absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 " style={{borderRadius : '50%' , cursor : 'pointer' , lineHeight : '77px'}} aria-hidden="true"></i>
                        </div>}
                            className="overflow-hidden lg:overflow-x-hidden md:overflow-x-hidden overflow-y-visible mr-5 text-center"
                            style={{height : '500px' ,width : '95%' , margin : '0 auto'}}
                        >
                            <Meta title={movie.tenPhim} description={movie.moTa} />
                            <NavLink to={`/detail/${movie.maPhim}`}>
                                <Button className="button__booking "  type="primary" style={{background:'black'}}>Mua vé</Button>
                            </NavLink>
                            </Card>
                        </div>
                    )
                })}
            </Slider>
    );
}

export default MovieSlider
