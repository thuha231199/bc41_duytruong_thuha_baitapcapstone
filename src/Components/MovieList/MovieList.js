import React , {useState , useEffect} from 'react'
import { movieSer } from '../../Services/movieService';
import MovieSlider from './MovieSlider';
import { MobileSearch } from '../../Layout/Responsive';
import SearchMobie from './SearchMobie';
import queryString from 'query-string';
function MovieList() {
    const [movieList, setMovieList] = useState([]) ; 
    useEffect(() => {
        const query = queryString.parse(window.location.search) ;
        let value = query.tenPhim ; 
        if (value) {
            let fetchSearchMovie = async() => {
                try {
                    let response = await movieSer.searchMovie(value) ; 
                    setMovieList(response.data.content) ;
                } catch (error) {
                    console.log(error);
                }
            }
            fetchSearchMovie() ; 
        }else {
            let fetchMovieList = async () => {
                try {
                    let response = await movieSer.getListMovie() ; 
                    setMovieList(response.data.content) ; 
                } catch (error) {
                    console.log(error);
                }
            }
            fetchMovieList() ; 
        }
        window.history.replaceState(null, null , '/');
    }, []) ; 
    return (
        <div id='lich__chieu' className='lg:w-3/5 lg:mx-auto md:w-11/12 md:mx-auto sm:mt-24 mt-24'>
            <MobileSearch>
                <SearchMobie/>
            </MobileSearch>
            <MovieSlider movieList = {movieList}/>
        </div>
    )
}

export default MovieList