import React from 'react'
import { listTrailer } from './dataTrailer';
import 'animate.css';
import ModalPlayer from '../Modal/Modal';
import { useDispatch } from 'react-redux';
import { handleOpenModal } from '../../Toolkits/modalSlice';
function SlideItem({item , index}) {
    let dispatch = useDispatch() ; 
    const contentStyle = {
        width : '100%' , 
        height : '70vh' ,
        color: '#fff',
        textAlign: 'center',
    };
    let handleOpen = (url) => {
        dispatch(handleOpenModal(url)) ; 
    }
    return (
        <div className='relative' style={contentStyle}>
            <img className='w-full h-full absolute top-0 left-0' src={item.hinhAnh} alt="banner" />
            <div className='w-full h-full flex justify-center items-center slide__movie absolute top-0 left-0'>
                <i onClick={() => handleOpen(listTrailer[index])} className="fa fa-play-circle text-5xl w-20 h-20 border transition hover:scale-125 button__play" style={{borderRadius : '50%' , cursor : 'pointer' , lineHeight : '77px'}} aria-hidden="true"></i>
            </div>
        </div>
    )
}

export default SlideItem