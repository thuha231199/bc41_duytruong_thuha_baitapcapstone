import { Carousel } from 'antd';
import React from 'react' ; 
import { useState ,useEffect } from 'react';
import SlideItem from './SlideItem';
import { movieSer } from '../../Services/movieService';
import SearchMovie from './SearchMovie';
import { Default } from '../../Layout/Responsive';

function Carousels() {
    const [listBanner, setListBanner] = useState([]) ; 
    useEffect(() => {
        let fetchBanner = async () => {
            try {
                let response = await movieSer.getListBanner() ; 
                setListBanner(response.data.content) ; 
                
            } catch (error) {
                console.log(error);
            }
        }
        fetchBanner() ; 
    }, []) ; 
    let renderSlideCarousel = () => {
        return listBanner.map((item , index) => {
            return <SlideItem index = {index} key={index} item = {item}/>
        })
    };
    return (
        <Default>
            <div style={{height : '70vh'}} className='w-full relative'>
                <Carousel className='mt-20 absolute top-0 left-0' autoplay effect='fade' dotPosition='bottom' easing='linear'>
                    {renderSlideCarousel()}
                </Carousel>
                <SearchMovie/>
            </div>
        </Default>
    )
}

export default Carousels ; 