import React from 'react'

export default function FooterDesktop() {
  return (
    <div id='footer'>
      <div className='lowerFooter container px-16 py-7 flex justify-between align-bottom'>
        <div><img className='w-32 object-contain' src="./images/download.jfif" alt="" /></div>
        <div className='text-white text-center text-xs px-10 leading-5'>
          <h6 className='mb-2'>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h6>
          <p>Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam. <br />
              Giấy chứng nhận đăng ký kinh doanh số: 0101659783, <br />
              đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp. <br />
              Số Điện Thoại (Hotline): 1900 545 436</p>
        </div>
        <div><img className='w-32 object-contain' src="./images/daThongBao-logo.png" alt="" /></div>
      </div>
    </div>
  )
}
