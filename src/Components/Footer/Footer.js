import React from 'react'
import { Desktop, LargeDesktop, Mobile, Tablet } from '../../Layout/Responsive'
import FooterLargeDesktop from './FooterLargeDesktop'
import FooterDesktop from './FooterDesktop'
import FooterTablet from './FooterTablet'
import FooterMobile from './FooterMobile'

export default function Footer() {
  return (
    <div>
      <LargeDesktop>
        <FooterLargeDesktop/>
      </LargeDesktop>
      <Desktop>
        <FooterDesktop/>
      </Desktop>
      <Tablet>
        <FooterTablet/>
      </Tablet>
      <Mobile>
        <FooterMobile/>
      </Mobile>
    </div>
  )
}
