import React from 'react'
import { useParams } from 'react-router-dom'
import { useState , useEffect } from 'react';
import { movieSer } from '../../Services/movieService';
import moment from 'moment';
import { Button , Progress , Rate, Tabs } from 'antd';
import { useDispatch } from 'react-redux';
import { handleOpenModal } from '../../Toolkits/modalSlice';
const onChange = (key) => {
  };
function DetailMovie() {
    let dispatch = useDispatch() ;
    let {id} = useParams() ; 
    const [movie, setMovie] = useState({}) ; 
    const [theaterMovie, setTheaterMovie] = useState([]) ; 
    useEffect(() => {
        let fetchDetailedMovie = async () => {
            try {
                let response1 = await movieSer.getDetailedMovie(id) ; 
                let response2 = await movieSer.getTheaterDetailedMovie(id) ; 
                setTheaterMovie(response2.data.content.heThongRapChieu) ; 
                setMovie(response1.data.content) ; 
            } catch (error) {
                console.log(error);
            }
        }
        fetchDetailedMovie() ; 
    
    }, [])
    let handleOpen = (url) => {
        dispatch(handleOpenModal(url)) ; 
    }
    let renderLichChieuPhim = () => {
        return theaterMovie.map((heThongRap , index)=> {
            return {
                key : heThongRap.maHeThongRap , 
                label : (
                    <div className='logoTheater'>
                    <img src={heThongRap.logo} alt="" className='w-10 object-contain'/>
                </div>
                ) , 
                children : (
                    heThongRap.cumRapChieu.map((cumRap , index)=> {
                        return (
                            <div key={index}>
                                <h2 className='text-lg text-black-500'>{cumRap.tenCumRap}</h2>
                                <div className='grid grid-cols-4 gap-x-5'>
                                    {cumRap.lichChieuPhim.map((date)=> {
                                        return <a style={{textDecoration : 'none'}} className='showtime hover:font-bold border p-2 rounded' href='#' key={date.maLichChieu}>
                                        <span className='text-black'>{moment(date.ngayChieuGioChieu).format("DD-mm-yyyy ")}</span>
                                        ~
                                        <span className='text-black'> {moment(date.ngayChieuGioChieu).format("hh:mm")}</span>
                                    </a>
                                    })}
                                </div>
                            </div>
                        )
                    })
                )
                
            }
        })
    }
    return (
        <div className='mx-auto mb-20 lg:w-3/5 md:w-11/12 sm:w-full w-full flex flex-wrap justify-between py-40'>
            <div className="title__movie flex">
                <div className='w-1/3 h-64 mr-5 rounded overflow-hidden'>
                    <img className='w-full h-full' src={movie.hinhAnh} alt="detail_image" />
                </div>
                <div className='lg:w-2/3 space-y-5'>
                    <h2 className='text-2xl text-black'>{movie.tenPhim}</h2>
                    <span className='block'>{moment(movie.ngayKhoiChieu).format('L')}</span>
                    <Button type='primary' style={{background:'gray'}}>Mua vé</Button>
                    <i onClick={() => {handleOpen(movie.trailer)}} className="fa fa-play-circle text-black text-5xl block" style={{cursor : 'pointer'}} aria-hidden="true"></i>
                </div>
            </div>
            <div className="rating__movie">
                <Progress size={150} type="circle" format={(percent) => `${percent/10} Đ`} percent={movie.danhGia*100}strokeColor={{'0%': '#108ee9','100%': '#87d068',}}/><br/>
                <Rate disabled defaultValue={5} />
            </div>
            <div className='tab__movie flex-grow-0 basis-full py-10'>
                <Tabs style={{width : 760}} tabPosition='left' items={renderLichChieuPhim()} defaultActiveKey='1' onChange={onChange}/>
            </div>
        </div>
    )
}

export default DetailMovie