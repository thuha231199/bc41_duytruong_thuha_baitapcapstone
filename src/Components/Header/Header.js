import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderUserInfo from './HeaderUserInfo';
import { NavLink } from 'react-bootstrap';

function Headers() {
  return (
    <>
      {['lg'].map((expand) => (
        <Navbar fixed='top' key={expand} expand={expand} style={{background : 'rgb(255, 255, 255 , .9)'}} className="mb-3 h-20 shadow-md font-bold z-50">
          <Container fluid>
            <Navbar.Brand href="/"><img src="https://demo1.cybersoft.edu.vn/logo.png" className='w-48' alt="logo" /></Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  CyberFlix
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body className='items-center lg:space-x-10'>
                <Nav className="justify-content-end flex-grow-1 pe-3 ">
                  <NavLink className='mr-5 border-b-2 pb-6 border-black lg:border-b-0 nav_link ' href="#lich__chieu">Lịch Chiếu</NavLink>
                  <NavLink className='mr-5 border-b-2 pb-6 border-black lg:border-b-0 nav_link ' href="#tabsMovie">Cụm Rạp</NavLink>
                  <NavLink className='mr-5 border-b-2 pb-6 border-black lg:border-b-0 nav_link' href="#tabsNews">Tin Tức</NavLink>
                  <NavLink className='mr-5 border-b-2 pb-6 border-black lg:border-b-0 nav_link' href="#footer">Ứng dụng</NavLink>
                </Nav>
                <HeaderUserInfo/>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  );
}

export default Headers;

