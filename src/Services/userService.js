import { https } from "./config"

export const userServ = {
    registerUser : (userInfo) => {
        return https.post('/api/QuanLyNguoiDung/DangKy' , userInfo) ; 
    } , 
    loginUser : (values) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap' , values) ; 
    }
}