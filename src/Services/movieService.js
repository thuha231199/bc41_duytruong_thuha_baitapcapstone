import { https } from "./config"

export const movieSer = {
    // * Các service api liên quan tới movie trên trang user 
    getListBanner : () => {
        return https.get('/api/QuanLyPhim/LayDanhSachBanner')
    },
    getMovieTheater: () => {
        return https.get('/api/QuanLyRap/LayThongTinLichChieuHeThongRap')
    } , 
    getListMovie : () => {
        return https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07')
    } , 
    getDetailedMovie : (maPhim) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    } , 
    getTheaterDetailedMovie : (maPhim) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`)
    } , 
    searchMovie : (tenPhim) => {
        return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09&tenPhim=${tenPhim}`)
    }
}