import axios from "axios"

export const articleServ = {
    // *Các service api liên quan tới mục article trên trang homepage
    getArticleDienAnh: () => {
        return axios({
            url: 'https://60b9f19280400f00177b744b.mockapi.io/ArticlesDienAnh02',
            method: 'GET',
        })
    },
    getArticleReview: () => {
        return axios({
            url: 'https://60babc8f42e1d0001761ff84.mockapi.io/ArticlesReview02',
            method: 'GET',
        })
    },
    getArticleKhuyenMai: () => {
        return axios({
            url: 'https://60babc8f42e1d0001761ff84.mockapi.io/ArticlesKhuyenMai02',
            method: 'GET',
        })
    }
}

