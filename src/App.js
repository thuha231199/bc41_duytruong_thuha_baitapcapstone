import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import { userRoute } from './routes/userRoute';
import Spinner from './Components/Spinner/Spinner';

function App() {
  return (
      <BrowserRouter>
        <Spinner/>
        <Routes>
          {userRoute.map((item , index) => {
            return <Route key={index} path={item.path} element={item.component}/>
          })}
        </Routes>
      </BrowserRouter>
  );
}

export default App;
