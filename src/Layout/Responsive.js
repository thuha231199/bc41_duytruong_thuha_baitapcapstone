import { useMediaQuery } from 'react-responsive'

export const LargeDesktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 1281 })
  return isDesktop ? children : null
}
export const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 958, maxWidth: 1280 })
  return isDesktop ? children : null
}
export const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 601, maxWidth: 957 })
  return isTablet ? children : null
}
export const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 600 })
  return isMobile ? children : null
}

export const MobileSearch = ({ children }) => {
  const isMobileSearch = useMediaQuery({ maxWidth: 768 })
  return isMobileSearch ? children : null
}

export const Default = ({ children }) => {
  const isNotMobile = useMediaQuery({ minWidth: 768 })
  return isNotMobile ? children : null
}
