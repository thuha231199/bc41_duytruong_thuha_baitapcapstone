import React from 'react'
import Headers from '../Components/Header/Header'

function LayoutRegLogin({Component}) {
    return (
        <div className='layout__reglogin min-h-screen'>
            <Headers/>
            <div className='flex justify-center items-center h-full w-full py-40'>
                <Component/>
            </div>
        </div>
    )
}

export default LayoutRegLogin